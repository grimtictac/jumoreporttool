#!/bin/bash

timestamp=`date +%Y%m%d_%H%M%S`
outfolder="out/$timestamp"
mkdir -p $outfolder

sep="============================================================================================="

printf "\nJUMO REPORT TEST SUITE\n"
printf "\n$sep\n"

printf "\nEXPECT PASS: Basic Validation\n\n\n"   ; python JumoReportTool.py test/test_validate_in.csv      $outfolder/test_validate_out.csv     ; printf "\n$sep\n"
printf "\nEXPECT FAIL: Wrong Header Field\n\n\n" ; python JumoReportTool.py test/test_fail_wrongheader.csv $outfolder/test_fail_wrongheader.csv ; printf "\n$sep\n"
printf "\nEXPECT FAIL: Parsing NonNumeric\n\n\n" ; python JumoReportTool.py test/test_fail_notnumber.csv   $outfolder/test_fail_notnumber.csv   ; printf "\n$sep\n"

printf "\nBENCHMARK: Verbose\n"  ;time python JumoReportTool.py test/test_big_in.csv $outfolder/test_big_out.csv --verbose ; printf "\n\n"
printf "\nBENCHMARK: LoadOnly\n" ;time python JumoReportTool.py test/test_big_in.csv $outfolder/test_big_out.csv --loadonly; printf "\n\n"
printf "\nBENCHMARL: Nowrite\n"  ;time python JumoReportTool.py test/test_big_in.csv $outfolder/test_big_out.csv --nowrite ; printf "\n\n"
