import sys

fname = sys.argv[1]
r     = int(sys.argv[2]) + 1

monthnames = ["Jan", 
              "Feb", 
              "Mar", 
              "Apr", 
              "May",
              "Jun",
              "Jul",
              "Aug",
              "Sep",
              "Oct",
              "Nov",
              "Dec"]

with open(fname, 'w') as f:
    f.write("MSISDN,Network,Date,Product,Amount\n")

    networkname_root = "Network "
    productname_root = "Loan Product "

    for nn in range(1,r):

        networkname = "'" + networkname_root + str(nn) + "'"

        for pn in range(1,r):
            
            prodcutname = "'" + productname_root + str(pn) + "'"

            for mn in range(12):

                month = monthnames[mn]

                for dn in range(1,r):

                    day = str(dn)
                    if len(day) < 2 : day = '0' + day


                    date = "'" + day+"-"+month+"-2016'"

                    f.write( "27729554427" + "," +
                             networkname     + "," +
                             date            + "," +
                             prodcutname     + "," +
                             "1000.00"       + "\n" )

