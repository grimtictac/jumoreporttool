import csv 
import sys
import pprint

FLUSH_THROTTLE=10000
DOT_THROTTLE  =1000

def num_for_monthname(m):
    ml = m.lower()

    if( ml[0:3] == 'jan' ): return 1
    if( ml[0:3] == 'feb' ): return 2
    if( ml[0:3] == 'mar' ): return 3
    if( ml[0:3] == 'apr' ): return 4
    if( ml[0:3] == 'may' ): return 5
    if( ml[0:3] == 'jun' ): return 6
    if( ml[0:3] == 'jul' ): return 7
    if( ml[0:3] == 'aug' ): return 8
    if( ml[0:3] == 'sep' ): return 9
    if( ml[0:3] == 'oct' ): return 10
    if( ml[0:3] == 'nov' ): return 11
    if( ml[0:3] == 'dec' ): return 12

    return 0 # unknown strings will sort to top


def load_data_file( filename, verbose=False ):
    if( verbose ): sys.stdout.write("Loading\n")

    d = csv.DictReader(open(filename))


    if( verbose ): 
        sys.stdout.write("Loading Done!\n") 
        sys.stdout.flush()

    return d

def process_data( data, verbose=False ):
    results = {} 

    if(verbose): 
        sys.stdout.write("Processing")
        sys.stdout.flush()

    lineNumber = 2 # the header is line 1
    
    try:

      for row in data:
          n = row['Network']
          p = row['Product']
          a = row['Amount' ]
          d = row['Date'].split('-')
          m = d[1] + d[2]

          if( n not in results ):
              results[n] = {} 

          if( p not in results[n] ):
             results[n][p] = {}

          if( m not in results[n][p] ):
              results[n][p][m] = 0

          results[n][p][m] += float(a)

          if(verbose and (lineNumber%DOT_THROTTLE==0)): 
              sys.stdout.write(".")
              if( lineNumber % FLUSH_THROTTLE == 0): sys.stdout.flush()

          lineNumber += 1

    except KeyError as ke:
        raise Exception("Missing field [%s] on line [%i]" % (ke,lineNumber) )
    except ValueError as ve:
        raise Exception("Line [%i] %s" % (lineNumber,ve) )

    return results


def write_results_to_stream( results, outstream, verbose=False ):
    
    if(verbose): 
        sys.stdout.write("\nWriting")
        sys.stdout.flush()

    outstream.write( "Network,Product,Month,Total\n" )

    i = 0

    for n in sorted( results ):
      for p in sorted( results[n] ):
          for m in sorted( results[n][p], key=num_for_monthname ):
            outstream.write( n + "," + 
                             p + "," + 
                             m + "," + 
                             str( results[n][p][m] ) + 
                             "\n" )
            i += 1
            
            if(verbose and (i%DOT_THROTTLE==0)): 
                sys.stdout.write(".")
                if( i % FLUSH_THROTTLE == 0): sys.stdout.flush()

if __name__ == "__main__":
  in_filename  = sys.argv[1]
  out_filename = sys.argv[2]
  
  verbose  = False
  loadonly = False
  nowrite  = False 

  if( len(sys.argv) > 3):
      if( sys.argv[3] == "--verbose"  ):
        verbose  = True
      if( sys.argv[3] == "--loadonly" ):
        loadonly = True
      if( sys.argv[3] == "--nowrite"  ):
        nowrite  = True
      if( sys.argv[3] == "--help"     ):
          sys.stdout.write("--verbose --loadonly --nowrite")
          sys.exit()

  d = load_data_file( in_filename, verbose )

  if( not loadonly ):
    r = process_data( d, verbose )
  
    if( not nowrite ):
        with open( out_filename, 'w' ) as outfile:
            write_results_to_stream( r, outfile, verbose )



#pp = pprint.PrettyPrinter()
#pp.pprint(networks)

