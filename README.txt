# JumoReportTool

Usage: 
    python JumoReportTool.py <infile> <outfile> [options]

Options:
    
    --help      show options
    --verbose   display progress
    --loadonly  load only
    --nowrite   load and process only

Loads csv data from <infile> in the following format:

MSISDN,Network,Date,Product,Amount
27729554427,'Network 1','12-Mar-2016','Loan Product 1',1000.00
27722342551,'Network 2','16-Mar-2016','Loan Product 1',1122.00
27725544272,'Network 3','17-Mar-2016','Loan Product 2',2084.00
27725326345,'Network 1','18-Mar-2016','Loan Product 2',3098.00
27729234533,'Network 2','01-Apr-2016','Loan Product 1',5671.00
27723453455,'Network 3','12-Apr-2016','Loan Product 3',1928.00
27725678534,'Network 2','15-Apr-2016','Loan Product 3',1747.00
27729554427,'Network 1','16-Apr-2016','Loan Product 2',1801.00

and transforms it into the following:

Network,Product,Month,Total
'Network 1','Loan Product 1',Mar2016',1000.0
'Network 1','Loan Product 2',Mar2016',3098.0
'Network 1','Loan Product 2',Apr2016',1801.0
'Network 2','Loan Product 1',Mar2016',1122.0
'Network 2','Loan Product 1',Apr2016',5671.0
'Network 2','Loan Product 3',Apr2016',1747.0
'Network 3','Loan Product 2',Mar2016',2084.0
'Network 3','Loan Product 3',Apr2016',1928.0

which is the accumlated loan amount across each netork, each product and each month.

# Language Choice

This script is implemented in Python, which is a natural choice for simple, batch oriented data transforms. It's quite
ubiquitous across almost any platform and most programers are comfortable maintaining it. The only issue with Python is 
the whole 2 vs 3 situation, but I've avoided any print or print() statements (which is one of the most annoying differences...) 
so this script actually runs fine in both. I haven't tested it in windows, but I am only using very basic standard library 
functionality so there shouldn't be any platform issues.

# Benchmark Results

Benchmark data was generated using gen_test_data.py with a parameter of 30. This means 30 networks, each with 30 products 
12 months and 30 days of the month. This results in 324001 rows of data (wc -l test_big_in.csv).

Benchmark results are extracted from the output of the run_tests.sh script: 

Load                     0.246s
Load, Process           32.586s
Load, Process, Write    34.094s

We can see that the vast majority of the time is spent in the process phase (process_data() method). This makes sense
because there is probably a lot of allocation as the dictionaries grow. This is just a hunch though and I haven't tested it.
If that were the case I would try to pre-allocate all the network and prodcut name dictionaries so that they don't have to 
keep shuffling data around.

As far as the algorithm is concerned, it is a single pass over the data, so I don't see much room for improvement there...
I could be wrong of course. If performance were a critical issue I would consider switching to numpy.

# TODOs

- implement python argparse, the current way of handling options is clunky

- re-raise exceptions properly in Python 3, because currently it displays an annoying message:
  "During handling of the above exception, another exception occurred:"
  Apparently this is a feature... https://docs.python.org/3.3/whatsnew/3.3.html#pep-409-suppressing-exception-context
  If so, then why do you need a PEP to disable it?

- use csv.DictWriter (even though the triple nested loop is so simple...)

- load data into sqlite to allow for more flexible queries. It's built into python so it's almost a no-brainer.

- do a more thorough timing analysis to generate a graph for gen_test_data.py with parameters of 10,20,30,40,50 etc

- add proper profiling and timing code

- raise an excoetion on a bad month name, the only one I can think of would be ValueError, but that is already handled as 
  a float parsing error. I would need to specialise the excpetion handling a bit more.

- currently it fails on the first error it finds, which leads to a bit of a tedious run, fix, run, fix loop if there are lots of 
  errors. I'd prefer to accumulate all the errors and print them at the end. Maybe have a validate command flag.

# CHANGELOG
v0.1    - Core functionality complete
v0.2    - Add some basic testing and validation
v0.3    - Benchmarking
v0.4    - More complete testing and documentation
